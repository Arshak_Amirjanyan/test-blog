<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/register', 'AuthController@register');
Route::post('/login', 'AuthController@login');
Route::middleware('auth:api')->group(function () {
    Route::prefix('blogs')->group(function () {
        Route::get('', 'BlogController@index')->middleware('permission:blog-list');
        Route::post('/{blog}/comment', 'BlogController@comment')->middleware('permission:post-comment');
        Route::post('/{blog}/react/{reaction}', 'BlogController@react')->middleware('permission:blog-react');
        Route::post('/create', 'BlogController@store')->middleware('permission:blog-create');
        Route::get('/{blog}', 'BlogController@show')->middleware('permission:blog-view');
        Route::put('/{blog}', 'BlogController@update');
        Route::delete('/{blog}', 'BlogController@destroy');
    });
    Route::prefix('categories')->group(function () {
        Route::get('/search', 'CategoryController@search')->middleware('permission:blog-list');
        Route::get('', 'CategoryController@index')->middleware('permission:category-list');
        Route::post('/create', 'CategoryController@store')->middleware('permission:category-create');
        Route::get('/{category}', 'CategoryController@show')->middleware('permission:category-view');
        Route::delete('/{category}', 'CategoryController@destroy')->middleware('permission:category-delete');
    });
    Route::prefix('users')->group(function () {
        Route::get('/{user}/{blog}', 'UserController@getUsersBlogs');
    });
});
