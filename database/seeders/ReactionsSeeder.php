<?php

namespace Database\Seeders;

use App\Models\MadeReaction;
use App\Models\Reaction;
use Illuminate\Database\Seeder;

class ReactionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $reactions = MadeReaction::REACTIONTYPES;

        foreach ($reactions as $reaction){
            Reaction::create(["type" => $reaction]);
        }
    }
}
