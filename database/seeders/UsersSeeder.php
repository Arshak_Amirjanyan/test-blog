<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adminData = [
            'name' => 'Admin',
            'email' => 'admin@admin.com',
            'password' => bcrypt('admin123')
        ];

        $adminRole = Role::where('name', 'admin')->first();
        $admin = User::create($adminData);
        $admin->assignRole($adminRole->id);

        $userRole = Role::where('name', 'user')->first();
        $users = User::factory(5)->create()->each(function ($user) use ($userRole) {
            $user->assignRole($userRole->id);
            $user->media()->create(["media_path" => substr($user->photo_url, -15)]);
            $user->save();
        });
    }
}
