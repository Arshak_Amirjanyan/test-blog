<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RolesAndPermissionsSeeder extends Seeder
{
    /**
     * all permissions
     */
    protected const PERMISSIONS = [
       'listBlogs' => 'blog-list',
       'viewBlog' => 'blog-view',
       'createBlog' => 'blog-create',
       'editBlog' => 'blog-edit',
       'deleteBlog' => 'blog-delete',
       'listCategories' => 'category-list',
       'viewCategory' => 'category-view',
       'createCategories' => 'category-create',
       'editCategories' => 'category-edit',
       'deleteCategories' => 'category-delete',
       'postComment' => 'post-comment',
       'reactBlog' => 'blog-react'
    ];


    /**
     * permissions by role
     */
    protected const ROLE_PERMISSIONS = [
        'admin' => [self::PERMISSIONS],
        'user' => [
            self::PERMISSIONS['listBlogs'],
            self::PERMISSIONS['viewBlog'],
            self::PERMISSIONS['createBlog'],
            self::PERMISSIONS['listCategories'],
            self::PERMISSIONS['viewCategory'],
            self::PERMISSIONS['postComment'],
            self::PERMISSIONS['reactBlog']
        ],
    ];


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (self::PERMISSIONS as $permission) {
            Permission::updateOrCreate(['name' => $permission]);
        }

        foreach (self::ROLE_PERMISSIONS as $role => $permissions){
           $createdRole = Role::updateOrCreate(['name' => $role, 'guard_name' => 'api']);
           foreach ($permissions as $permission) {
               $createdRole->givePermissionTo($permission);
           }
        }
    }
}
