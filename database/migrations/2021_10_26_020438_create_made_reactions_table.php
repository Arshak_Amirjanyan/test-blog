<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMadeReactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('made_reactions', function (Blueprint $table) {

            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('reaction_id');
            $table->unsignedBigInteger('blog_id');

            $table->foreign('user_id')
                ->references('id')
                ->on('users');
            $table->foreign('reaction_id')
                ->references('id')
                ->on('reactions');
            $table->foreign('blog_id')
                ->references('id')
                ->on('blogs');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('made_reactions');
    }
}
