<?php

namespace App\DataTransferObjects\User;

use App\Models\User;
use Spatie\DataTransferObject\DataTransferObject;

class UserDTO extends DataTransferObject
{
    /**
     * @var int
     */
    public int $id;

    /**
     * @var string
     */
    public string $name;

    /**
     * @var string
     */
    public string $email;

    /**
     * @var
     */
    public $photo;

    /**
     * @param User $user
     * @return UserDTO
     */
    public static function fromModel(User $user): UserDTO
    {
        return new self ([
            'id' => $user->id,
            'name' => $user->name,
            'email' => $user->email,
            'photo' => $user->photo_url,
        ]);
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return parent::toArray();
    }
}
