<?php

namespace App\DataTransferObjects\User;

use Illuminate\Support\Collection;

class UserDtoCollection extends Collection
{
    /**
     * @param array $data
     */
    public function __construct($data = []) {
        parent::__construct();
        foreach ($data as $d){
            $this->items[]= UserDTO::fromModel($d);
        }
    }
}
