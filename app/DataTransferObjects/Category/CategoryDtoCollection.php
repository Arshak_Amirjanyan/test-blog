<?php

namespace App\DataTransferObjects\Category;

use Illuminate\Support\Collection;

class CategoryDtoCollection extends Collection
{
    /**
     * @param array $data
     */
    public function __construct(array $data = []){
        parent::__construct();
        foreach ($data as $d){
            $this->items[]= CategoryDTO::fromModel($d);
        }
    }

}
