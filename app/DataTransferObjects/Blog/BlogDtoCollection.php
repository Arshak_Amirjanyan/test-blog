<?php

namespace App\DataTransferObjects\Blog;

use Illuminate\Support\Collection;

class BlogDtoCollection extends Collection
{

    /**
     * @param array $data
     */
    public function __construct($data = []) {
        parent::__construct();
        foreach ($data as $d){
            $this->items[]= BlogDTO::fromModel($d);
        }
    }

}
