<?php

namespace App\DataTransferObjects\Blog;

use App\DataTransferObjects\Comment\CommentDtoCollection;
use App\Models\Blog;
use App\Models\MadeReaction;
use Illuminate\Support\Collection;
use Spatie\DataTransferObject\DataTransferObject;

final class BlogDTO extends DataTransferObject
{
    /**
     * @var int
     */
    public int $id;

    /**
     * @var string
     */
    public string $name;

    /**
     * @var string
     */
    public string $description;

    /**
     * @var
     */
    public $photo;

    /**
     * @var object
     */
    public object $categories;

    /**
     * @var array
     */
    public array $reactionsCount;

    public $comments;
    /**
     * @param Blog $blog
     * @return BlogDTO
     */
    public static function fromModel(Blog $blog): BlogDTO
    {
        return new self ([
            'id' => $blog->id,
            'name' => $blog->name,
            'description' => $blog->description,
            'photo' => $blog->photo_url,
            'categories' => $blog->categories,
            'reactionsCount' => ['likes' => $blog->madeReactionTypeCount(MadeReaction::LIKE),
                                'dislikes' => $blog->madeReactionTypeCount(MadeReaction::DISLIKE)],
            'comments' => new CommentDtoCollection($blog->comments)
        ]);
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return parent::toArray();
    }
}
