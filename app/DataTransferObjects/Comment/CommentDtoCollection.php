<?php

namespace App\DataTransferObjects\Comment;

use App\DataTransferObjects\User\UserDTO;
use Illuminate\Support\Collection;

class CommentDtoCollection extends Collection
{
    /**
     * @param array $data
     */
    public function __construct($data = [])
    {
        parent::__construct();
        foreach ($data as $d) {
            $this->items[] = CommentDTO::fromModel($d);
        }
    }
}

