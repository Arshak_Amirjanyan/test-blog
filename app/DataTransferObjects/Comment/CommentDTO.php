<?php

namespace App\DataTransferObjects\Comment;

use App\DataTransferObjects\User\UserDTO;
use Spatie\DataTransferObject\DataTransferObject;

class CommentDTO extends DataTransferObject
{

    public UserDTO $user;

    /**
     * @var string
     */
    public string $comment;

    public static function fromModel($comment): CommentDTO
    {
        return new self ([
            'user' => UserDTO::fromModel($comment),
            'comment' => $comment->pivot->comment
        ]);
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return parent::toArray();
    }
}
