<?php

namespace App\Services\Files;

use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

class ImageService implements FileServiceInterface
{
    public function makeFile(string $dataToProcess)
    {
        $image = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $dataToProcess));
        return $image;
    }

    public function getExtension(string $imageBase64)
    {
        return explode('/', mime_content_type($imageBase64))[1];
    }

    /**
     * @param $file
     * @param string $disk
     * @param string $name
     * @param string $extension
     * @return string
     */
    public function storeFile($file, string $disk, string $name): string
    {
        Storage::disk($disk)->put($name, $file,["public"]);
        return Storage::disk($disk)->url($name);
    }

    public function deleteFile(string $disk, $file){
        Storage::disk($disk)->delete($file);
    }
}
