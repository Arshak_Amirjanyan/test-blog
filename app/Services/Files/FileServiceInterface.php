<?php

namespace App\Services\Files;

interface FileServiceInterface
{
    public function makeFile(string $dataToProcess);
    public function storeFile($file, string $disk,  string $name);
}
