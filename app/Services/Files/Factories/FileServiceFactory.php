<?php

namespace App\Services\Files\Factories;

use App\Services\Files\ImageService;

class FileServiceFactory
{
    public static function getFileService(string $type){
        if ($type == 'image'){
            return new ImageService();
        }
    }
}
