<?php

namespace App\Services;

use App\Models\Blog;
use App\Services\Files\Factories\FileServiceFactory;
use Illuminate\Database\Eloquent\Model;
use PhpParser\Node\Expr\AssignOp\Mod;

class ImagableService
{
    /**
     * @var MediaService
     */
    protected MediaService $mediaService;

    /**
     * @param MediaService $mediaService
     */
    public function __construct(MediaService $mediaService){
        $this->mediaService = $mediaService;
    }

    public function handleImage(Model $model, string $imageBase64)
    {
        $imageService = FileServiceFactory::getFileService('image');
        $imageFile = $imageService->makeFile($imageBase64);
        $extension = $imageService->getExtension($imageBase64);
        $modelName = class_basename(get_class($model));
        $imageName = $modelName.$model->id.'photo'.'.'.$extension;
        $image_url = $imageService->storeFile($imageFile, 's3', $imageName, $extension);
        $model->photo_url = $image_url;
        $model->save();
        $this->mediaService->attachMedia($model, $imageName);

    }

    public function handleDelete(Model $model){
        $imageService = FileServiceFactory::getFileService('image');
        $imageService->deleteFile('s3', $model->media->media_path);
    }
}
