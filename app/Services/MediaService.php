<?php

namespace App\Services;

use App\Models\Media;
use Illuminate\Database\Eloquent\Model;

class MediaService
{
    /**
     * @param Model $model
     * @param $media_path
     * @return Media
     */
    public function attachMedia(Model $model, $media_path): Media
    {
        $model->media ? $model->media()->update(["media_path" => $media_path])
            : $model->media()->create(["media_path" => $media_path]);

        return $model->media()->first();
    }

    /**
     * @param Model $model
     * @return Model
     */
    public function removeMedia(Model $model): Model
    {
        $media = $model->media()->all();
        $model->media()->delete($media);
        return $model;
    }
}
