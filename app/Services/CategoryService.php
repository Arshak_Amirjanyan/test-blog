<?php

namespace App\Services;

class CategoryService
{

    /**
     * Recursively get all the blogs having a category or its child categories without duplicates
     *
     * @param $categories
     * @return array
     */
    public function getBlogsOfCategoryWithSubcategories($categories): array
    {
        if (sizeof($categories) == 0) {
            return [];
        }

        $blogsByCategory = $this->getBlogsOfCategoryWithSubcategories($categories[0]->all_children);
        foreach ($categories as $category) {
            if (sizeof($category->blogs) != 0) {
                foreach ($category->blogs as $blog) {
                    $blogsByCategory[$blog->id] = $blog;
                }
            }
        }

        return $blogsByCategory ?? [];
    }

    //get blogs like a tree(you can uncomment this and test)
    /*
    public function getBlogsOfCategoryWithSubcategories($categories)
    {
      //  dd($categories[0]->allChildren[0]->allChildren[0]->allChildren);
        if (sizeof($categories) == 0) {
            return [];
        } else {
            $dxk = $this->getBlogsOfCategoryWithSubcategories($categories[0]->all_children);
            if (sizeof($dxk) != 0) {
                $blogs[$categories[0]->name] = $dxk;
            }
            foreach ($categories as $category) {
                if (sizeof($category->blogs) != 0) {
                    $blogs[$category->name][] = $category->blogs;
                }
            }
            //dd($blogs);
            return $blogs ?? [];
        }

    }
    */
}
