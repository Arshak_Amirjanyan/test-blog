<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Category extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'parent_id',
        'name',
    ];

    protected $hidden = ["pivot"];

    public function getAllChildrenAttribute()
    {
        return $this->allChildren()->get();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent(): BelongsTo
    {
        return $this->belongsTo(Category::class, 'parent_id');
    }

    /**
     * returns all the direct children of that category(categories with parent_id of the current category)
     *
     * @return HasMany
     */
    public function directChildren(): HasMany
    {
        return $this->hasMany(Category::class, 'parent_id');
    }

    /**
     * returns all children of the given category recursively(the complete subtree with the root as the current category)
     *
     * @return HasMany
     * $categories = Category::with('allChildren')->whereNull('parent_id')->get();
     */
    public function allChildren(): HasMany
    {
        return $this->directChildren()->with('allChildren');
    }

    /**
     * @return BelongsToMany
     */
    public function blogs(): BelongsToMany
    {
        return $this->belongsToMany(Blog::class, 'blogs_categories', 'category_id', 'blog_id');
    }
}
