<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\Pivot;

class MadeReaction extends Pivot
{
    use HasFactory;

    public const LIKE = 1;
    public const DISLIKE = 2;

    public const REACTIONTYPES = [self::LIKE, self::DISLIKE];

    public $timestamps = false;
    protected $table = 'made_reactions';
    protected $fillable = ["user_id", "reaction_id", "blog_id"];

    /**
     * @return BelongsTo
     */
    public function reaction(): BelongsTo
    {
        return $this->belongsTo(Reaction::class, 'reaction_id');
    }

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * @return BelongsTo
     */
    public function blog(): BelongsTo
    {
        return $this->belongsTo(Blog::class, 'blog_id');
    }

    /**
     * @param $query
     * @param int $blogId
     * @return Builder
     */
    public function scopeMadeReactionToBlog(Builder $query, int $blogId): Builder
    {
        return $query->where('blog_id', $blogId);
    }

    /**
     * @param Builder $query
     * @param int $user_id
     * @return Builder
     */
    public function scopeReactedUser(Builder $query, int $user_id): Builder
    {
        return $query->where('user_id', $user_id);
    }

    /**
     * @param string $type
     * @return Builder
     */
    public function scopeReactionsByType(Builder $query, int $type): Builder
    {
        return $query->where('reaction_id', $type);
    }
}
