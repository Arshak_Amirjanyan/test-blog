<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphOne;

class Blog extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'user_id',
        'description',
        'photo_url'
    ];

    protected $hidden = ["pivot"];


    /**
     * @return BelongsToMany
     */
    public function categories(): BelongsToMany
    {
        return $this->belongsToMany(Category::class, 'blogs_categories', 'blog_id', 'category_id');
    }

    /**
     * @return BelongsToMany
     */
    public function comments(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'comments', 'blog_id', 'user_id')
            ->withPivot('comment');
    }

    /**
     * @return HasMany
     */
    public function madeReactions(): HasMany
    {
        return $this->hasMany(MadeReaction::class, 'blog_id');
    }

    /**
     * @param int $type
     * @return mixed
     */
    public function madeReactionsByType(int $type)
    {
        return $this->madeReactions()->reactionsByType($type);
    }

    /**
     * @param int $type
     * @return int
     */
    public function madeReactionTypeCount(int $type): int
    {
        return $this->madeReactions()->reactionsByType($type)->count();
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function media(): MorphOne
    {
        return $this->morphOne(Media::class, 'mediaholder');
    }

    /**
     * @param int $user_id
     * @return mixed
     */
    public function getUserReaction(int $user_id)
    {
        return $this->madeReactions()->reactedUser($user_id)->with('reaction')->first();
    }
}
