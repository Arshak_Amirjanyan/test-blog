<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;

class Media extends Model
{
    use HasFactory;

    protected $fillable = ["media_path"];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function mediaholder(): MorphTo
    {
        return $this->morphTo();
    }
}
