<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Testing\Fluent\Concerns\Has;

class Reaction extends Model
{
    use HasFactory;

    protected $fillable = ["type"];

    /**
     * @return BelongsToMany
     */
    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'made_reactions', 'reaction_id', 'user_id');
    }

    /**
     * @return BelongsToMany
     */
    public function blogs(): BelongsToMany
    {
        return $this->belongsToMany(Blog::class, 'made_reactions', 'reaction_id', 'blog_id');
    }

    /**
     * @return HasMany
     */
    public function madeReactions(): HasMany
    {
        return $this->hasMany(MadeReaction::class, 'reaction_id');
    }

}
