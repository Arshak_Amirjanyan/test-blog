<?php

namespace App\Http\Responses;

use Illuminate\Contracts\Support\Responsable;
use Symfony\Component\HttpFoundation\Response;

class ApiResponse implements Responsable
{
    private $data;
    private $status;

    /**
     * @param $data
     * @param int $status
     */
    public function __construct($data, $status = 200)
    {
        $this->data = $data;
        $this->status = $status;
    }

    /**
     * @return array
     */
    public function constructResponseData(): array
    {
        return $this->status === 200 ? $this->responseData = ['data' => $this->data] : $this->responseData = ['error' => $this->data];
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function toResponse($request): Response
    {
        $responseData = $this->constructResponseData();
        return response()->json($responseData, $this->status)->withHeaders(['Content-Type' => 'application/json', 'Accept'=>'application/json']);
    }
}
