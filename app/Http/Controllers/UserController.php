<?php

namespace App\Http\Controllers;

use App\DataTransferObjects\Blog\BlogDtoCollection;
use App\Http\Responses\ApiResponse;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * @param User $user
     * @return ApiResponse
     */
    public function getUsers(User $user): ApiResponse
    {
        $blogs = $user->blogs;
        return new ApiResponse(new BlogDtoCollection($blogs));
    }
}
